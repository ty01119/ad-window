window.onload = function() {
  let PlayerType = undefined
  let { ipcRenderer } = require("electron")
  // 页面的两个窗口tab
  let PlayFrameOne = document.querySelectorAll("#PlayFrame-one .PlayFrame")
  let PlayFrameTwo = document.querySelectorAll("#PlayFrame-two .PlayFrame")
  let DOMarr = [...PlayFrameOne, ...PlayFrameTwo]
  // 页面播放状态
  let PlayerNumber = new Map()
  let PlayerList = [{}, {}]
  let PlayerDOM = undefined
  let MasterTime = undefined
  // 配置
  ipcRenderer.on("Configure", function(event, arg){
    let { Host, Router, Player, GPU } = arg
    PlayerType = Player
  })
  // 当前节目需要播放的次数
  // 播放到次数之后跳过
  function PlayerNumberInit(data, callback) {
    try {
      if (data.number !== "auto" && typeof data.number === "number") {
        let indexOf = PlayerNumber.get(data.name)
        if (indexOf) {
          let newIndexOf = indexOf + 1
          PlayerNumber.set(data.name, newIndexOf)
          if (newIndexOf > data.number) {
            throw new Error()
          }
        } else {
          PlayerNumber.set(data.name, 1)
        }
      }
      callback(true)
    } catch(Error) {
      callback(false)
    }
  }
  // 预加载以及页面显示控制
  function InitWindow(doc, data) {
    for (let i = 0, PlayType = false; i < doc.length; i ++) {
      let v = doc[i]
      if (v.getAttribute("name") === "top") {
        v.style.opacity = 0
        v.style.display = "none"
        v.setAttribute("name", "")
        if (PlayerType == "embed" && v.getAttribute("video") == "true") {
          v.contentWindow.document.getElementById("video").pause()
        }
        setTimeout(function(){
          v.style.width = data.width + "%"
          v.style.height = data.height + "%"
          v.style.top = data.y + "%"
          v.style.left = data.x + "%"
          v.setAttribute("play", data.name)
          // 如果是视频页面 显示黑底
          if (data.type == "video") {
            if (PlayerType == "embed") {
              v.src = "video.html"
              v.onload = function(){
                if ("document" in v.contentWindow && v.contentWindow.document.getElementById("video") && "src" in v.contentWindow.document.getElementById("video")) {
                  if (data.path.startsWith("http") || data.path.startsWith("https")) {
                    v.contentWindow.document.getElementById("video").src = data.path
                  } else {
                    v.contentWindow.document.getElementById("video").src = "./cache/" + data.path
                  }
                }
              }
            } else {
              v.src = "back.html"
            }
            v.setAttribute("video", "true")
          } else {
            if (data.path.startsWith("http") || data.path.startsWith("https")) {
              v.src = data.path
            } else {
              v.src = data.path ? "cache/" + data.path : "back.html"
            }
            v.setAttribute("video", "false") 
          }
          v.setAttribute("playdata", JSON.stringify({
            path: data.path,
            width: data.width,
            height: data.height,
            x: data.x,
            y: data.y,
            volume: data.volume,
            time: data.time,
            number: data.number
          }))
        }, 2000)
      } else {
        if (v.getAttribute("video") != "true") {
          v.style.opacity = 1
          v.style.display = "block"
        } else {
          // 如果是视频 暂存节点状态
          // 等待主进程响应
          if (PlayerType == "os") {
            PlayerDOM = v
            PlayType = true
            ipcRenderer.send("PlayerVideo", JSON.parse(v.getAttribute("playdata")))
          } else
          if (PlayerType == "embed") {
            v.contentWindow.document.getElementById("video").play()
            v.style.opacity = 1
            v.style.display = "block"
          }
        }
        // 通知播放状态
        ipcRenderer.send("PlayerStatus", {
          name: v.getAttribute("play"),
          PlayerNumber
        })
        v.setAttribute("name", "top")
        if (typeof v.contentWindow.Reload === "function") {
          v.contentWindow.Reload(JSON.parse(v.getAttribute("playdata")))
        }
      }
      if (i === doc.length-1 && PlayType && PlayerType == "os") {
        // 锁定主进程
        clearInterval(MasterTime)
      }
    }
  }
  // 收到播放列表推送
  ipcRenderer.on("PlayerList", function(event, arg){
    // 先把播放列表格式化
    PlayerList = [[], []]
    for (let timeout = 0, endtime = 0, i = 0; i < arg.length; i++) {
      let v = arg[i]
      if (Array.isArray(v)) {
        v.forEach(function(v, i){
          if (i == 0) {
            PlayerList[0].push(v)
          } else {
            if (timeout == 0) {
              PlayerList[1].push(v)
            } else {
              PlayerList[1].push({ timeout })
              PlayerList[1].push(v)
              timeout = 0
              endtime = 0
            }
          }
        })
      } else {
        endtime += v.time
        timeout += v.time
        PlayerList[0].push(v)
      }
      if (i === arg.length - 1) {
        // 格式化完成
        endtime != 0 && PlayerList[1].push({ timeout: endtime })
        let LeftIndex = 0
        let RightIndex = 0
        let LeftTimeOut = true
        let RightTimeOut = true
        // 主流程时间抽的处理
        function StartTime() {
          // 右
          if (RightTimeOut) {
            let Init = PlayerList[1][RightIndex];
            RightTimeOut = false
            if (Init.timeout) {
              setTimeout(() => {
                if (RightIndex == PlayerList[1].length - 1) {
                  RightIndex = 0
                } else {
                  RightIndex++
                }
                RightTimeOut = true
              }, (Init.timeout - 1) * 1000)
            } else {
              PlayerNumberInit(Init, fn => {
                if (fn) {
                  InitWindow(PlayFrameTwo, Init)
                  setTimeout(() => {
                    if (RightIndex == PlayerList[1].length - 1) {
                      RightIndex = 0
                    } else {
                      RightIndex++
                    }
                    RightTimeOut = true
                  }, (Init.time - 1) * 1000)
                } else {
                  if (RightIndex == PlayerList[1].length - 1) {
                    RightIndex = 0
                  } else {
                    RightIndex += 2
                  }
                  RightTimeOut = true
                }
              })
            }
          }
          // 左
          if (LeftTimeOut) {
            let Init = PlayerList[0][LeftIndex]
            LeftTimeOut = false
            PlayerNumberInit(Init, fn => {
              if (fn) {
                InitWindow(PlayFrameOne, Init)
                setTimeout(() => {
                  if (LeftIndex == PlayerList[0].length - 1) {
                    ipcRenderer.send("PlayerListEnd", true)
                    LeftIndex = 0
                  } else {
                    LeftIndex++
                  }
                  LeftTimeOut = true
                }, (Init.time - 1) * 1000)
              } else {
                if (LeftIndex == PlayerList[0].length - 1) {
                  ipcRenderer.send("PlayerListEnd", true)
                  LeftIndex = 0
                } else {
                  LeftIndex += 2
                }
                LeftTimeOut = true
              }
            })
          }
        }
        // 播放器准备完成
        // 接触锁状态
        ipcRenderer.on("PlayerEnd", (event, arg) => {
          PlayerDOM.style.opacity = 0
          PlayerDOM.style.display = "none"
          MasterTime = setInterval(StartTime, 1000)
        })
        // 1/s 一次的定时器
        MasterTime = setInterval(StartTime, 1000)
      }
    }
  })
}