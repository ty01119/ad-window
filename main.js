"use strict"


// 加载依赖
const child_process = require("child_process")
const electron = require("electron")
const { app, BrowserWindow, ipcMain, crashReporter, shell } = electron
const fs = require("fs")
const os = require("os")
const path = require("path")
const url = require("url")
const http = require("http")
const querystring = require("querystring")
const cluster = require("cluster")


// 读取配置
const { 
  Host, 
  Router, 
  Player, 
  GPU, 
  UpdateTimeout 
} = JSON.parse(fs.readFileSync(__dirname + "/AD-window.json"))


// 关闭GPU加速
if (!GPU) {
  app.disableHardwareAcceleration()
}


// 全局对象
let Window = undefined
let playerCache = undefined
let MesterPlayList = undefined
let PlayListName = undefined
let OMXPlayerWindow = false
let Display = [0, 0]
let PlayerNumber = undefined
let PlayerTTYcmd = undefined
let PlayerListEffective = undefined
let PlayerTasks = undefined
let PlayerTimeOut = undefined


// 日志通知
function Notification(info) {
  try {
    Window.webContents.send("Notification", info)
  } catch(Error) {
    return
  }
}


// 启动播放器命令
function StartPlayerCmd(options) {
  try {
    let width = Display[0]
    let height = Display[1]
    let width_pix = width / 100
    let height_pix = height / 100
    let x = options.x * width_pix
    let y = options.y * height_pix
    let omx_width = options.width * width_pix + x
    let omx_height = options.height * height_pix + y
    return `omxplayer ${__dirname}/page/cache${options.path} --win ${x},${y},${omx_width},${omx_height} --vol ${options.volume || 0}`
  } catch(Error) {
    Notification({Error: Error.message})
  }
}


// 关闭播放器
function KillFork() {
  child_process.exec("killall -s 9 omxplayer.bin", (Error, stdout, stderr) => {
    if (Error) {
      Notification({Error: Error.message})
    }
  }) 
}


// 获取播放器进程状态
function GetPlayerInfo(callback) {
  child_process.exec("./control getstatus", (Error, stdout, stderr) => {
    if (Error) {
      Notification({Error: Error.message})
      callback(false)
      return
    }
    if (stdout === "Paused") {
      callback(false)
    } else
    if (stdout === "Playing") {
      callback(true)
    }
  })
}


// 运行命令
function RunPlayer(options) {
  child_process.exec(PlayerTTYcmd, (Error, stdout, stderr) => {
    if (Error) {
      Notification({Error: Error.message})
      OMXPlayerWindow = false
    } else {
      OMXPlayerWindow = true
    }
  })
}


// 启动播放器
function StartPlayer(options) {
  PlayerTTYcmd = StartPlayerCmd(options)
  if (OMXPlayerWindow) {
    void KillFork()
    OMXPlayerWindow = false
  }
  let AssertPlayer = setInterval(() => GetPlayerInfo(v => {
    if (v) {
      void KillFork()
      RunPlayer(options)
    } else {
      Window.webContents.send("PlayerEnd", options)
      setTimeout(() => {
        void KillFork()
        OMXPlayerWindow = false
      }, options.time * 1000)
      clearInterval(AssertPlayer)
    }
  }), 1000)
}


// 启动子进程下载资源
function StartFork(body) {
  let Play = JSON.parse(body)
  let Fork = child_process.fork(`${__dirname}/fork.js`)
  Fork.on("close", statusCode => {
    if (statusCode === 8) {
      PlayerTasks = Play.Tasks
      PlayerListEffective = Play.Effective
      MesterPlayList = Play.PlayerList
      if (Play.Effective === "now") {
        Window.webContents.send("PlayerList", MesterPlayList)
      }
    }
  })
  Fork.on("error", Error => {
    Notification({Error: Error.message})
  })
}


// 检查资源更新
function AssertFiles() {
  let width = Display[0]
  let height = Display[1]
  let display = { width, height }
  let hostname = os.hostname()
  let loadavg = os.loadavg()
  let uptime = os.uptime()
  let freemem = os.freemem()
  let totalmem = os.totalmem()
  let networkInterfaces = os.networkInterfaces()
  let req = http.request({
    hostname: Host,
    method: "POST",
    path: Router,
    headers: { "Content-Type": "text/json" }
  }, res => {
    res.on("error", Error => {return})
    if (res.statusCode === 200) {
      let body = ""
      res.on("data", data => {
        body += data.toString()
      })
      res.on("end", () => {
        fs.writeFile(`${__dirname}/configure.json`, body, Error => {
          !Error && StartFork(body)
        })
      })
    }
  })
  req.write(JSON.stringify({ 
    display, 
    hostname, 
    loadavg, 
    uptime, 
    freemem, 
    totalmem, 
    networkInterfaces, 
    PlayListName,
    PlayerNumber
  }))
  req.end()
  req.on("error", Error => {
    return
  })
}


// 初始化完成
void app.on("ready", () => {
  let { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
  let pathname = path.join(`${__dirname}/page/index.html`)
  fs.readFile(`${__dirname}/configure.json`, (Error, Data) => {
    if (!Error) {
      MesterPlayList = (JSON.parse(Data.toString())).PlayerList
    }
  })
  Window = new BrowserWindow({ 
    width: width,
    height: height, 
    frame: false, 
    fullscreen: true, 
    fullscreenable: true
  })
  Window.loadURL(url.format({ 
    pathname, 
    protocol: "file:", 
    slashes: true 
  }))
  // Window.webContents.openDevTools()
  Display[0] = width
  Display[1] = height
  // 轮询检查更新
  void setInterval(AssertFiles, UpdateTimeout)
  void AssertFiles()
  // 检查播放列表状态
  let PlayerListAssert = setInterval(() => {
    if (MesterPlayList) {
      setTimeout(function(){
        Window.webContents.send("PlayerList", MesterPlayList)
        Window.webContents.send("Configure", { Host, Router, Player, GPU })
      }, 3000)
      clearInterval(PlayerListAssert)
    }
  }, 1000)
  // 页面要求打开播放器
  void ipcMain.on("PlayerVideo", (event, options) => {
    StartPlayer(options)
  })
  // 已完成一个列表
  ipcMain.on("PlayerListEnd", (event, arg) => {
    if (PlayerListEffective === "auto") {
      Window.webContents.send("PlayerList", MesterPlayList)
    }
  })
  // 页面反馈播放状态
  void ipcMain.on("PlayerStatus", (event, init) => {
    PlayListName = init.name
    PlayerNumber = init.PlayerNumber
    if (PlayerTimeOut) {
      Window.webContents.send("PlayerList", PlayerTimeOut)
    }
  })
  // 检查播放列表的计划任务
  void setInterval(() => {
    let data = new Date()
    let Hours = data.getHours()
    let Minute = data.getMinutes()
    let Key = `${Hours.toString().length > 1 ? Hours : "0" + Hours}:${Minute.toString().length > 1 ? Minute : "0" + Minute}`
    if (PlayerTasks && Key in PlayerTasks) {
      PlayerTimeOut = PlayerTasks[Key]
    }
  }, 30000)
})