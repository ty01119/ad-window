// 子进程
// IO操作进程
// 不对主进程造成阻塞

"use strict"


// 加载依赖
const fs = require("fs")
const assert = require("assert")
const http = require("http")


// 全局变量
let dirname = `${__dirname}/page/cache`


/**
 * 读取文件
 * @param {*文件路径} path 
 */
function ReadFile(path) {
  return new Promise(callback => {
    fs.readFile(path, (Error, data) => {
      callback(Error ? false : data)
    })
  })
}


/**
 * 写入文件
 * @param {*文件路径} path 
 * @param {*数据} data 
 */
function WriteFile(path, data) {
  return new Promise(callback => {
    fs.writeFile(path, data, Error => {
      callback(Error ? false : true)
    })
  })
}


/**
 * 静态资源下载
 * @param {*远程地址} Path 
 * @param {*本地路径} FilePath 
 * @param {*文件名} FileName 
 */
function LoadFile(host, path) {
  return new Promise(async callback => {
    /**
     * 路径切割
     * 每次叠加
     */
    let dir = dirname
    let dirList = path.split("/")
    let length = dirList.length - 1
    /**
     * 循环新建路径
     */
    for (let i = 0; i < length; i++) {
      let pathValue = dirList[i]
      // 新建目录
      dir += "/" + pathValue
      fs.mkdir(dir, error => {
        return
      })
      // 循环新建目录完成
      if (i === length - 1) {
        /**
         * http 下载文件
         */
        http.get(host, req => {
          let FileBold = Buffer.alloc(0)
          /**
           * 在接收到数据的时
           * 同步写入到文件
           */
          req.on("data", data => {
            FileBold = Buffer.concat([FileBold, data], FileBold.length + data.length)
          })
          // 响应完成
          req.on("end", async () => {
            let W = await WriteFile(dirname + "/" + path, FileBold)
            callback(W ? { End: true } : { Error: true })
          })
          // 响应错误
          req.on("error", error => {
            callback({ Error: true })
          })
          // 响应超时
          req.on("timeout", () => {
            callback({ Error: true })
          })
        }).on("error", error => {
          callback({ Error: true })
        })
      }
    }
  })
}


/**
 * 入口
 * 读取配置
 * 下载资源
 */
void (async () => {
  // 读取配置
  let configure = await ReadFile(__dirname + "/configure.json")
  /**
   * 配置不存在
   * 退出进程
   */
  if (!configure) {
    process.exit(0)
    return
  }
  // 获取下载列表
  let { FilesList } = JSON.parse(configure.toString())
  if (!FilesList) {
    process.exit(0)
    return
  }
  // 开始下载
  let fileList = Object.keys(FilesList)
  /**
   * 循环队列
   */
  for (let i = 0; i < fileList.length; i++) {
    let host = fileList[i]
    let path = FilesList[host]
    let ReqFn = await LoadFile(host, path)
    /**
     * 如果出现错误
     * 重新添加到队列
     */
    if (ReqFn.Error) {
      fileList.push(host)
    } else 
    if (ReqFn.End) {
      /**
       * 队列完成
       * 退出进程
       * 退出码为8
       */
      if (i === fileList.length - 1) {
        process.exit(8)
        return
      }
    }
  }
})()