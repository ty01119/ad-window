AD-window
===
> Raspberry Pi ad window, of Electron<br>


配置 <`AD-window.json`>
---
* `Host` `{string}` 服务器的地址
* `Router` `{string}` 轮询的路由
* `Player` `{string}` 播放调用的方式 `{embed || os}`
  * `embed` : 调用嵌入的网页播放视频
  * `os` : 调用系统自带的播放器
* `UpdateTimeout` `{Data}` 轮询的间隔


轮询更新 <`HTTP POST`>
---
> request <`form body`> `JSON`
* `display`：
  * `width` `{number}` 设备的显示器宽度
  * `height` `{number}` 设备的显示器高度
* `hostname` `{stringr}` 设备名
* `loadavg` `{array}` 系统负载
* `uptime` `{number}` 系统运行时间
* `freemem` `{number}` 空闲内存
* `totalmem` `{number}` 总内存
* `networkInterfaces` `{object}` 网络接口信息
* `PlayListName` `{string}` 当前播放的节目
* `PlayerNumber` `{map}` 播放次数
> response <`statusCode === 200 / statusCode === 301`> `JSON`


播放列表说明
---
* `PlayListName` `{string}` 播放列表名
* `PlayerList` `{array}` 播放列表
* `FilesList` `{object}` 静态资源列表
* `Effective` `{string}` 生效的规则，`{auto || now}`, auto 为自动生效, now为现在生效
* `Tasks` `{array}` 计划任务，以时间为 Key, 播放列表为 Value 
---
> `{PlayerList} 播放列表内子配置`
* 单屏为 `{object}` 双屏为 `{array}`
* `name` `{string}` 广告名
* `time` `{number}` 单个广告播放时间
* `type` `{string}` 广告类型，只有两种 `html || video`
* `path` `{atring}` 广告的地址，可以为本地静态链接，也可以为远程连接
* `width` `{number}` 窗口宽度
* `height` `{number}` 窗口高度
* `x` `{number}` 窗口横向轴定位
* `y` `{number}` 窗口竖向轴定位
* `number` `{number || string}` 播放的次数，如果无限制为 `auto`
* `volume` `{number}` 视频的音量，当类型为视频时，此对象存在，默认为 100
---
> `{FilesList} 静态资源列表内子配置`
* 以键对只存在，远程下载地址 `key`，本地文件相对路径 `value` 
* `http url` `:` `local dirname` `{object}`


页面内API <获取页面开始播放事件>
---
* `window.Reload` 事件函数
``` js
window.Reload(function(param){
  // 此页面成为焦点，并开始显示
  // param 为当前节目单配置参数
  let { path, width, height, x, y, volume, time, number } = param
})
```